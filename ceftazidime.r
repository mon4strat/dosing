# This R file defines the core model (both C(t) and C_SS(t)), as well as the associated PK parameter distributions
source("model2cpt.R")
BWAdjustV1 <- function(ETA_V1, cov) {
  names(ETA_V1) <- c()
  BW <- cov[1]
  c(V1=21.2 * exp(ETA_V1) * (BW/70)**1) #[l]
}
BWAdjustV2 <- function(ETA_V2, cov) {
  names(ETA_V2) <- c()
  BW <- cov[1]
  c(V2=12.5*exp(ETA_V2) * (BW/70)**1) #[l]
}
BWAdjustQ <- function(ETA_Q, cov) {
  names(ETA_Q) <- c()
  BW <- cov[1]
  c(Q=14.6* exp(ETA_Q) * (BW/70))
}
AdjustCL <- function(ETA_CL, cov) {
  names(ETA_CL) <- c()
  BW <- cov[1]
  CRCL <- cov[2]
  RF2 <- CRCL / BW * 70
  RF2 <- RF2 / 100
  c(CL=3.48*RF2**(0.31) * exp(ETA_CL) * (BW/70)**0.75)
}
Parameters <- list(
  # Parameter SD is based on OMEGA from the model
  # We assume CV is calculated as CV = sqrt(OMEGA**2)
  # and not as CV = sqrt( exp(omega^2) -1 )
  ETA_V1=parameter( mu=0, sd=0.548, log=T, toValue=BWAdjustV1 ),
  ETA_V2=parameter( mu=0, sd=0.454, log=T, toValue=BWAdjustV2 ),
  ETA_Q=parameter( mu=0, sd=0, log=T, toValue=BWAdjustQ),
  ETA_CL=parameter( mu=0, sd=0.698, log=T, toValue=AdjustCL)
)
EPS_PROP <- 0.172
EPS_ADD <- 0