# Standard functions used for plotting
PLOT_POINTS <- 500

init_screen <- function(sideRows, sideColumns=1, ratio=0.6) {
par(bg = "white")
# split screen into
# MAIN  | par
close.screen(all.screens=T)
split.screen(matrix(
  c(# left, right, bottom, top
    0, ratio, 0, 1, #MAIN
    ratio, 1, 0, 1	# par
  ), byrow=T, ncol=4))
# split the parameter screen in X seperate screens, one per parameter
split.screen(c(sideRows, sideColumns), screen=2)
}

getTRange <- function(data) {
  T_MAX <- max(c( data@data$t * 1.2 , data@ebe$t * 1.2, sapply(data@events, function(x){x$t})))
  seq(0, T_MAX, length.out=PLOT_POINTS)
}

plot_real <- function(data, Model, xlim=NULL) {
  if(length(data@realParameters) > 0 ) {
    if(is.null(xlim)) {
      tRange <- getTRange(data)
      xlim <- range(tRange)
    } else {
      tRange <- seq(min(xlim), max(xlim), length.out=PLOT_POINTS)
    }
    lines(tRange, Model(tRange, data, data@realParameters), col='purple', type='l')
  }
}

plot_base <- function(title="", data, parameters, model, mic=NULL, ylim=NULL, xlim=NULL, showMean=T, showLegend=F, legendOrientation="bottomright", mar=NULL) {
  #T_MAX <- max(data@data$t) * 1.2
  if(!is.null(mar)) par(mar=mar)
  if(is.null(xlim)) {
    tRange <- getTRange(data)
    xlim <- range(tRange)
  } else {
    tRange <- seq(min(xlim), max(xlim), length.out=PLOT_POINTS)
  }
  
  mu <- sapply(parameters, function(x){x@mu})
  # plot mean model
  Mean <- model(tRange, data, mu)
  if(is.null(ylim)) ylim <- c(1, max(Mean)*3)
  
  # plot points
  if(length(data@ebe$t) > 0) {
    plot(data@ebe$t, data@ebe$x, xlab='t', ylab='Conc' , log="y" , main=title, ylim=ylim, xlim=xlim)
  } else {
    plot(tRange[Mean>0], Mean[Mean>0], xlab='t', ylab='Conc' , log="y" , main=title, ylim=ylim, xlim=xlim, pch=NA)
  }

  # and observation error
  if(length(data@ebe$t)!=0){
    eps_sd <- sqrt( EPS_ADD**2 + (EPS_PROP*data@ebe$x)**2 )
    arrows(x0=data@ebe$t, 
         y0=data@ebe$x + qnorm(0.025, 0, eps_sd), 
         y1=data@ebe$x + qnorm(0.975, 0, eps_sd), 
         code=3, 
         angle=90, 
         length=0.1 )
  }
  if(showMean) lines(tRange, Mean, col='blue')
  
  abline(h=mic, col='green')
  abline(h=SAFETY_CMAX, col='purple')
}

plot_legend <- function(filteredFields=c(), legendOrientation="bottomright") {
  legendProps <- list(
    key=c("mean", "original", "real", "obs", "pred", "pred_conf", "mic"),
    text=c(
      "Model (mean)", 
      "Prediction (original dose)",
      "Model (real)",
      "Observations (p=95%)",
      "Prediction",
      "Prediction (p=95%)",
      "4*MIC"),
    col=c("blue", "cyan", "purple",
          "black", "red", "red", "green"),
    lty=c(1, 1, 1,
          NA, 1, 2, 1),
    pch=c(NA, NA, NA,
          1, NA, NA, NA)
  )
  i <- which( legendProps$key %in% filteredFields)
  legendProps <- lapply(legendProps, function(x){ x[! (1:length(x) %in% i)] } )
  
  legend(x=legendOrientation,
         legend=legendProps$text,
         col=legendProps$col,
         lty=legendProps$lty,
         pch=legendProps$pch
  )
}

plot_model <- function(EstParameters, Model, data, col='red', xlim=NULL) {
  if(is.null(xlim)) {
    tRange <- getTRange(data)
  } else {
    tRange <- seq(min(xlim), max(xlim), length.out=PLOT_POINTS)
  }
  lines(tRange, Model(tRange, data, EstParameters), col=col, type='l')
}

plot_mc <- function(MCEstParameters, Model, data, xlim=NULL) {
  if(is.null(dim(MCEstParameters)))
    return(); # we have only one single MCEstParameter
  if(is.null(xlim)) {
    tRange <- getTRange(data)
  } else {
    tRange <- seq(min(xlim), max(xlim), length.out=PLOT_POINTS)
  }
  init_cluster()
  predict <- function(eta, tRange, myData){Model(tRange, myData, eta)}
  Pred <- M4SParApply(MCEstParameters, 1, predict, tRange, data)
  deciles <- apply(Pred, 1, function(x) {quantile(x, prob=c(0.025, 0.975))} )
  plot_mc2(tRange, deciles[1,], deciles[2,])
}

plot_mc2 <- function(tRange, q05, q95, col="red") {
  col1 <- col2rgb(col) / 255
  polygon(c(tRange, rev(tRange)), c(q05, rev(q95)), col=rgb(col1[1], col1[2], col1[3], 0.1), border=NA)
  lines(tRange, q05, col=col, lty=3)
  lines(tRange, q95, col=col, lty=3)
}

genDensity <- function(p, Covariates, mu=p@mu, sd=p@sd, valueRange) {
  densSample <- list(x=seq(-5*sd, 5*sd, length.out=100))
  densSample$y <- dnorm(densSample$x, 0, sd)
  if(p@log) {
    densSample$x <- mu + densSample$x
  } else {
    densSample$x <- mu * exp(densSample$x)
  }
  
  densSample$x <- p@toValue(densSample$x, Covariates)
  
  class(densSample) <- "density"
  return(densSample)
}

plot_parameter <- function(p, pName, EstParameter, SDParameter, Covariates, Real=NULL, CovariatesPop=Covariates, xlim=NULL, col=c("blue", "red"), mar=c(2, 0, 3, 0)) {
  #bottom, left, top, right
  par(mar=mar)
  parDensity <- genDensity(p, Covariates, EstParameter, SDParameter)
  popDensity <- genDensity(p, CovariatesPop, valueRange=EstParameter+c(-5*SDParameter, 5*SDParameter))
  
  if(is.null(xlim)) {
    xlim = c(parDensity$x, popDensity$x)
    xlim = range( xlim [ xlim > 0 ] )
  }
  ylim1 = max(parDensity$y)
  ylim2 = max(popDensity$y)
  ylim = c(0, min(ylim1, ylim2))
  
  EstValue <- p@toValue(EstParameter, Covariates)
  
  plot(popDensity, xlim = xlim, ylim = ylim, xlab = '',
       main=names(EstValue),
       panel.first = grid(),
       log="x")
  col1 <- col2rgb(col[1])/255
  col2 <- col2rgb(col[2])/255
  polygon(popDensity, density = -1, col = rgb(col1[1], col1[2], col1[3],0.2))
  lines(parDensity, col=col[2])
  polygon(parDensity, density=-1, col=rgb(col2[1], col2[2], col2[3], 0.1))
  
  abline(v=EstValue, col=col[2], lwd=2)
  abline(v=p@toValue(p@mu, CovariatesPop), col=col[1], lwd=2)
  if(!is.null(Real)) abline(v=p@toValue(Real, Covariates), col='purple', lwd=2)
}
