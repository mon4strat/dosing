# Utility functions and classes
parameter <- setClass(
  Class="parameter", 
  #slots=c(
  representation=list(
    mu="numeric",           # mu of population
    sd="numeric",           # sd of population
    log="logical",          # is the parameter already in the log domain?
    toValue="function"),    # convert to a covariate-adjusted value - function(x, covariates)
  prototype=list(
    mu=0,
    sd=Inf,
    log=T,
    toValue=function(x, cov) {x}
  ) )
parameter <- function(...) {
  new("parameter", ...)
}

resultsData <- setClass("resultsData", 
                        #slots=c(
                        representation=list(
	subject="numeric",          # Subject ID
	realParameters="numeric",   # Actual ETA's used to generate data
	data="data.frame",          # data-frame with columns 't' and 'x'
	ebe="data.frame",           # data-frame with columns 't' and 'x' (only these are used to estimate)
	covariates="numeric",       # named vector with covariates
	events="list",              # list of 'event' objects (see subclasses in model definition)
	treatmentTimes="numeric",
	treatmentDoses="numeric",
	treatmentInfDuration="numeric"
	),             
	prototype=list(
	subject=1,
	realParameters=numeric(),
	data=data.frame(),
	ebe=data.frame(),
	covariates=numeric(),
	events=list(),
	treatmentTimes=numeric(),
	treatmentDoses=numeric(),
	treatmentInfDuration=numeric()
))
resultsData <- function(...) {
  new("resultsData", ...)
}

generateFile <- function(times, Parameters, Covariates, Events, eps_sd, eta=NULL) {
  myData <- data.frame()
  mu <- sapply(Parameters, function(x){x@mu})
  sd <- sapply(Parameters, function(x){x@sd})
  
  if(is.null(eta))
    eta <- rnorm(length(mu), mu, sd)
  names(eta) <- names(Parameters)
  
  newFile <- resultsData(
    subject=1,
    realParameters=eta,
    data=myData,
    ebe=myData,
    covariates=Covariates,
    events=Events
  )
  Prediction <- Model(times, newFile, eta)
  myData <- data.frame(t=times, x=Prediction)
  if(length(times) > 0)
    myData$x <- Prediction * exp( rnorm(length(Prediction), 0, eps_sd) )
  
  resultsData(
    subject=1,
    realParameters=eta,
    data=myData,
    ebe=myData,
    covariates=Covariates,
    events=Events
  )
}

loadFile <- function(resultsFile, selectedSubject=NULL, filterData=NULL, restrictObservations=NULL) {
	results <- read.csv(resultsFile, row.names=NULL)
	# pick a random subject
	if(is.null(selectedSubject)) selectedSubject <- sample(results$subject,1)
	results <- subset(results, subject==selectedSubject)

	# extract real parameter values
	mu <- sapply(Parameters, function(x) x@mu)
	RealParameters <- tryCatch({
		unlist(na.omit(subset(results, eventType=="OBS", select=names(mu)))[1,]
		)
	}, error=function(e){
		numeric()
	})
	# extract (t, observation) pairs
	data <- subset(results, eventType=="OBS", select=c('t', 'C'))
	data <- na.omit(data)
	names(data) <- c('t', 'x')
	data <- data[!duplicated(data$t), ]

	if(!is.null(filterData)) {
		if(max(filterData) > length(data$t)) stop("filterData is larger than results file")
		data <- data[filterData, ]
	}
	dataEbe <- data
	if(!is.null(restrictObservations)) {
		if(max(restrictObservations) > length(data$t)) stop("restrictObservations is larger than results file")
		dataEbe <- dataEbe[restrictObservations, ] #restrict nr of observations
	}

	# extract covariates
	Covariates <- tryCatch({
		na.omit(subset(results, T, select=names(Covariates)))[1,]
	}, error=function(e){
		stop("Could not find covariates in RESULTS file\n", e)
	})
	Covariates <- as.numeric(Covariates)

	# extract dosing scheme
	events <- with(results, {
	 lapply( which(eventType=='TMT'), function(i) { dose(t[i], DoseAmount[i], DoseDuration[i]) })
	})

	return(resultsData(
							subject=selectedSubject,
							realParameters=RealParameters,
							data=data,
							ebe=dataEbe,
							covariates=Covariates,
							events=events
							))
}