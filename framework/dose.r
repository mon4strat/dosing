
findD <- function(estimates, tau, duration, data, SSModel, DoseUpperLimit, MIC) {
  names(estimates) <- names(Parameters)
  throughDelta <- function(Dose) {
    C_at_through <- SSModel(c(0), data, estimates, tau, Dose, duration)
    C_at_through - 4*MIC
  }
  
  # maybe the root is outside of the search index? In the case, just return the closest limit...
  limits <- c(0, DoseUpperLimit)
  limitValues <- c( throughDelta(limits[1]), throughDelta(limits[2]) )
  if(any(is.nan(limitValues))) return(NaN) # model cannot be evaluated
  if(sign(limitValues[1]) == sign(limitValues[2])) {
    if( min(abs(limitValues)) == abs(limitValues[1]) ) {
      return(limits[1])
    } else {
      return(limits[2])
    }
  }
  result <- uniroot(throughDelta, limits)
  return( result$root )
}

findTau <- function(estimates, dose, duration, data, SSModel, TauLowerLimit, TauUpperLimit, MIC) {
  # find when C_at_through goes 
  names(estimates) <- names(Parameters)
  throughDelta <- function(tau) {
    C_at_through <- SSModel(c(0), data, estimates, tau, dose, duration)
    C_at_through - 4*MIC
  }
  # maybe the root is outside of the search index? In the case, just return the closest limit...
  limits <- c(TauLowerLimit, TauUpperLimit)
  limitValues <- c( throughDelta(limits[1]), throughDelta(limits[2]) )
  if(any(is.na(limitValues))) return(limits[2])
  if(sign(limitValues[1]) == sign(limitValues[2])) {
    if( min(abs(limitValues)) == abs(limitValues[1]) ) {
      return(limits[1])
    } else {
      return(limits[2])
    }
  }
  result <- uniroot(throughDelta, limits)
  return( result$root )
}

generateSSFile <- function(D, myFile, SSTau, SSDuration, PLOT_DOSES=5, respectInputDoses=TRUE) {
  adjustedFile <- myFile
  tCutoff <- max(c(0, myFile@ebe$t))
  eventTimes <- sapply(myFile@events, function(e){ e$t} )
  if(respectInputDoses)
    tCutoff <- max(tCutoff, eventTimes)
  lastDose <- max( c(0, eventTimes[ eventTimes <= tCutoff]) ) + SSTau
  adjustedFile@events <- c(
    myFile@events[eventTimes <= tCutoff], 
    lapply(seq(lastDose, lastDose+PLOT_DOSES*SSTau, by=SSTau), 
           function(t){
             dose(t, D, SSDuration)
           }))
  adjustedFile
}




findTrough <- function(estimates, data, Model, MIC) {
  # find when C_at_through goes 
  names(estimates) <- names(Parameters)
  throughDelta <- function(t) {
    C_at_through <- Model(c(t), data, estimates)
    C_at_through - 4*MIC
  }
  # maybe the root is outside of the search index? In the case, just return the closest limit...
  limits <- c(0, 48)+max( sapply(data@events, function(e){e$t + e$duration} ) )
  limitValues <- c( throughDelta(limits[1]), throughDelta(limits[2]) )
  if(any(is.nan(limitValues))) return(limits[1])
  if(sign(limitValues[1]) == sign(limitValues[2])) {
    if( min(abs(limitValues)) == abs(limitValues[1]) ) {
      return(limits[1])
    } else {
      return(limits[2])
    }
  }
  result <- uniroot(throughDelta, limits)
  return( result$root )
}
