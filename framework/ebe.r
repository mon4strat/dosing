##########
# Emperical Bayesian Estimation with standard R optim() function
# It should be possible to supply the hessian/jacobian for the optim() function, based on the
# numerical derivatives of the algebraic equations of the model.
#
# This will further improve the speed.
##########
clusterExportClass <- function(cluster, names) {
	assignClass <- function(name, definition) {
		assign(name, setClass(name, definition), envir=globalenv())
	}
	for(className in names) {
		clusterCall(cluster, assignClass, className, getClass(className))
	}
}

M4SParApply <- function(mySource, order, myFunction, 
            ...) {
  if(!length(dim(mySource))) {
    return(myFunction(mySource, ...))
  }
  if(is.null(Cluster)) init_cluster()
  if(anyNA(Cluster)) {
    apply(mySource, order, myFunction, ...)
  } else {
    parApply(Cluster, mySource, order, myFunction, ...)
  }
}

#func <- model(t=c(), covariates=c(), template=templateFunction)
#func is a function(eta) with eta=c(eta1,eta2,..)=the subject estimates
#ebe(model=func, mu=NULL, var=NULL, sd=NULL, data=c(), opts=list(ERROR="ABSOLUTE"))
ofvPop <- function(Estimates, parameters) {
  sdZeroPars = sapply(parameters, function(x) x@sd == 0)
  
  # only consider non-zero SD values
  parameters = parameters[!sdZeroPars]
  Estimates = Estimates[!sdZeroPars]
  
  logPars = sapply(parameters, function(x) x@log)
  mu = sapply(parameters, function(x) x@mu)
  sd = sapply(parameters, function(x) x@sd)
  
  OFV <- sum(
          dnorm(
            Estimates[logPars], 
            mu[logPars], 
            sd[logPars], 
            log=T)
          )
  # for the linear parameters, rework back to the original log
  mu <- mu[!logPars]
  sd <- sd[!logPars]
  Estimates <- Estimates[!logPars]
  
  Estimates <- log( Estimates / mu )
  OFV <- OFV +
    sum(
      dnorm(Estimates, 0, sd, log=T)
    )
  
  OFV <- -1*OFV
  return(OFV)
}

ofvPred <- function(Estimates, model, parameters, data) {
	Prediction = model(data$t, Estimates)
  eps_sd <- sqrt( EPS_ADD**2 + (EPS_PROP*Prediction)**2 )
	OFV <- -1*dnorm(Prediction - data$x, 0, eps_sd, log=T)
	#OFV <- -1*dnorm(Prediction, data$x, eps_sd, log=T) #absolute error
	OFV <- sum(OFV)
	return(OFV)
}

ofv <- function(Estimates, model, parameters, data) {
  OFV = 0
  if(dim(data)[1] > 0)
    OFV = ofvPred(Estimates, model, parameters, data)
  OFV = OFV + ofvPop(Estimates, parameters)
  #cat(paste("Returning OFV = ", OFV, '\n'))
  return(OFV)
}

ClusterCores <- NULL
Cluster <- NULL
init_cluster <- function(cores=NULL, parameters=NULL, classes=NULL) {
	if(is.null(cores)) cores <- detectCores()
	if(is.na(cores)) cores<-2
	ClusterCores <<- cores

	if(is.null(Cluster)) Cluster <<- makeCluster(cores)
  if(anyNA(Cluster)) return()
	clusterEvalQ(Cluster, library(deSolve))
	clusterEvalQ(Cluster, library(MASS))
  if(is.null(parameters))
    parameters <- c(
      "Parameters",
      "EPS_PROP",
      "EPS_ADD",
      "EtaToParameters",
      "ebe",
      "ofv",
      "Model",
      "SSModel",
      "findD",
      "findTau",
      "ULOQ",
      "LLOQ",
      "M4S_FindDose",
      "M4S_FindCmax",
      "M4S_FindCmin",
      "M4SParApply")
  if(is.null(classes))
    classes <- c("parameter", "resultsData")
	clusterExport(Cluster, parameters)
	clusterExportClass(Cluster, classes)
}

##
# ebe expects a model function(tPred, eta), data as data-frame
# 
ebe <- function(model, parameters, data=NULL,
	       	init=NULL, plotDebug=NULL) {
  # do not estimate SD zero parameters, they should be fixed
  sdZeroPars <- sapply(parameters, function(x) x@sd==0)
  mu <- sapply(parameters, function(x) x@mu)
  
	objective <- function(Estimates) {
    fullEstimates <- mu
    fullEstimates[!sdZeroPars] <- Estimates
		ofv(fullEstimates, model, parameters, data)
	}
	if(is.null(init)) init <- mu
  parscale <- mu[!sdZeroPars]
  parscale[parscale==0] <- 1
	result <- optim(init[!sdZeroPars], objective, method="Nelder-Mead", control=list(maxit=20000, parscale=parscale))
  
  estimates <- mu
  estimates[!sdZeroPars] <- result$par
  return(estimates)
}

ebe_sd <- function(estimates, model, parameters, data=NULL) {
  sdZeroPars <- sapply(Parameters, function(x) x@sd == 0)
  mu <- sapply(Parameters, function(x) x@mu)
  
  ll <- function(eta) {
    myEta <- mu
    myEta[!sdZeroPars] <- eta
    -1*ofv(
      myEta, 
      model=model, 
      parameters=parameters, 
      data=data
    )
  }
  
  myHessian <- hessian(ll, estimates[!sdZeroPars])
  FIM <- -1 * myHessian # Officially E(Hessian), but not necessary when using numeric methods
  myCovMatrix <- solve(FIM)
  
  # transfer back to a full matrix
  covMatrix <- diag(x=0, length(Parameters))
  colnames(covMatrix) <- names(Parameters)
  rownames(covMatrix) <- names(Parameters)
  # transfer covMatrix to full covMatrix
  i2 <- 1
  for(i1 in which(!sdZeroPars)) {
    j2 <- 1
    for(j1 in which(!sdZeroPars)) {
      covMatrix[i1, j1] <- myCovMatrix[i2, j2]
      j2 <- 1+1
    }
    i2 <- 1+1
  }
  
  return(covMatrix)
}
