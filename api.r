## This file contains the API for the dosing algorithm
## We make no assumptions on the environment this is run in, but ask to provide all required data as arguments.
## Everything uses the 'M4S' prefix, to make it perfectly clear what is stored in the environment

# Inputs:
# An INPUT object, containing:
# - Covariates CrCL [mL/min] and BW [kg]
# - Treatment times, doses and infusion duration: relative to t0. Times and duration in [h], dose in [mg].
# - Observation times: all observation times. There must be a time '0' in here, which is considered the starting time of the analysis t0.
# - Observations: the observed blood concentration, in mg/L.
#      * In the case of the first dosing recommendation, the observations and observationTimes will be something like this:
#          observationTimes=c(0, 4, 8)
#          observations=c(0, 64.38, 23.48)
#          treatmentTimes=c(2.5)
#          treatmentDoses=c(2000)
#          treatmentInfDuration=c(0.5)
#      This means that we chose an arbitrary point t0 (e.g. at day 1 - 22:00) where we know the
#      concentration is 0. There was an infusion at 00:30 of 2g.
#      Observations occurred at day2 02:00 and day2 06:00.
#      * In the case of a later dosing recommendation without instability, this might look like this:
#          observationTimes=c(0, 36, 40)
#          observations=c(0, 84.38, 43.48)
#          treatmentTimes=c(2.5, 10.5, 18.5, 26.5, 34.5)
#          treatmentDoses=c(2000, 1500, 1500, 1500, 1500)
#          treatmentInfDuration=c(0.5, 0.5, 0.5, 0.5, 0.5)
#      The same 't0' is kept (day 1 - 22:00). The full treatment history is present (every 8 hours).
#      For the observations, the observation on t0 remains included (=0). We throw away the observations made at day2 02:00 and day2 06:00.
#      We only keep the observations made at day3 10:00 and day3 14:00; throwing away those at day2 02:00 and day2 06:00.
# 
#      * In the case of a later dosing recommendation with instability, we select a new 't0'.
#          observationTimes=c(0, 3, 6)
#          observations=c(73.2, 53.38, 23.48)
#          treatmentTimes=c(3.5)
#          treatmentDoses=c(1500)
#          treatmentInfDuration=c(0.5)
#      In practice, this will be the time at which the first sample is taken. Therefore, we will have to now take 3 samples.
#      We also throw away all treatment information before that t0.
# 
#      In this case, suppose an instability occurred at day4 09:00 (e.g. we performed kidney dialysis).
#      We take a first observation at day4 09:30. This becomes t0=0.
#      We take a second observation at day4 12:30 (t=3). We administer 1500mg at day4 13:00.
#      We take a final observation at day4 15:30.
#      This is a little bit like a new patient, but one that begins already at a specific concentration.
# Extra information for the dosing recommendation:
# - The current DOSE, TAU and TInf that is followed.
#   You could obtain this either from the default treatment started (2g every 8h @ 0.5h) or from the latest dosing recommendation.
#   These should correspond to one of the available recommendation options. 1.5g every 7h is not a correct input.
#   The correct input should be:
#      Dose: 1000 or 2000
#      Tau: 6, 8, 12 or 24
#      TInf: 0.5 or 3
#   If not, then the option "prefer the current treatment" will not work. This will also be the case if the previous recommended treatment
#   is not present in the possibilities (e.g. if a dose of 3g or higher was recommended previously).
# - MIC: the targeted MIC level. The actual trough target will be 4*MIC.
# - N, the number of iterations to be used when estimating. Recommendation: N=1000 or higher.
# 
# CONFIGURATION:
# In principle, some configuration is possible.
# - Dosing possibilities: currently a permutation of all combinations of dose [1g, 2g], Tau [6h, 8h, 12h, 24h] and TInf [0.5h, 3h].
# - Safety cutoff: 80% certainty
# - Efficacy cutoff: 90% certainty
# - Ft target: 100% of time above target
# 
# PROCESSING:
# - The input object is used for estimation of PK parameters. What comes out is an "estimate" and a "covMatrix".
# - The dose recommendation uses these intermediary results to calculate a recommendation.
# 
# OUTPUT:
# A recommendation, with the following elements:
# - Dose
# - Tau
# - TInf
# - pSafety: the probability that the recommended dosing is safe
# - pEfficacy: the probability that the recommended dosing is efficient
# - comment: the reason for picking this recommendation
# Comment can be one of:
# - Safe and efficient
# We found a good one in the possibilities
# - Discrete continuous infusion
# All possibilities have a huge CMax and a very low CMin. This patient has a very low V1 and a very high CL, and can only be treated with a continuous infusion.
# We provide a continuous infusion of 1g,2g,3g,4g,5g, etc. every 3h.
# - Exact continuous infusion
# All possibilities have a huge CMax and a very low CMin. This patient has a very low V1 and a very high CL, and can only be treated with a continuous infusion.
# It is not possible to provide a rounded (multiple of 1g) continuous infusion every 3h.
# Either the dose required is much smaller (<1g @ 3h), or the range is so specific that e.g. 3g is too low and 4g is too high.
# - Highest Efficacy
# There are no efficient regimes, all CMin is below 4*MIC.
# The highest dosing is not safe, but something else is safe. Take the safe regimen with the highest predicted CMin
# - Higher dose than normal
# There are no efficient regimes, all CMin is below 4*MIC.
# However, the highest dosing is safe. We have therefore kept increasing the dose until we hit efficiency and safety.
# - Next step is unsafe
# There are no efficient regimes, all CMin is below 4*MIC.
# However, the highest dosing is safe. We have therefore kept increasing the dose.
# Suddenly, it was not safe anymore, so we stopped at the highest dose that was still safe. It is not efficient though...
# - ERROR: Even a dose of 32000mg is still safe and not efficient, stopping the search...
# There are no efficient regimes, all CMin is below 4*MIC.
# However, the highest dosing is safe. We have therefore kept increasing the dose.
# However, we reached 32g and still no efficacy. Something is wrong, we should not be recommending this...
# - Safe regime only found at very low dose, patient in renal failure?
# 1g q24h is not safe.
# We tried 500mg q24h @3h, 1g q48h @3h and 500mg q24h @3h. One of these was safe, so we recommend that.
# - ERROR: Could not find any safe regimen!
# 1g q24h is not safe.
# We tried 500mg q24h @3h, 1g q48h @3h and 500mg q24h @3h, and even those were not safe!
# - Safe. Highest efficacy.
# There is no overlap between safe and efficient options. We select the safe one with the highest predicted efficacy.

require(methods)
require(deSolve)
require(MASS)
require(tcltk)
require(parallel)
require(numDeriv)
require(plyr)
source('framework/util.r')
source('framework/ebe.r')
source("framework/graphical.r")
source("framework/dose.r")

# This function returns a version number, based on the date
# Version numbers can be naturally sorted
M4S_Version <- function() { "20180829.1405" } #YYYYMMDD.hhmm

M4S_DefaultEstimate <- function() {
  return( sapply(Parameters, function(p){p@mu}))
}

M4S_DefaultCovMatrix <- function() {
  return( diag( sapply(Parameters, function(p){p@sd}) ) )
}

M4S_MachineRecommendation <- function(
  observationTimes,
  observations, 
  covariates, 
  treatmentTimes,
  treatmentDoses,
  treatmentInfDuration,
  Dose,
  Tau,
  TInf,
  MIC,
  N
  ) {
 tryCatch({
    set.seed(0) #always set the same random seed, so calculations will always give the same outcome
    myInput <- M4S_GenerateInput(observationTimes=observationTimes, 
                                 observations=observations, 
                                 covariates=covariates, 
                                 treatmentTimes=treatmentTimes, 
                                 treatmentDoses=treatmentDoses, 
                                 treatmentInfDuration=treatmentInfDuration
                                   )
    myEstimate <- M4S_Estimate(myInput)
    myCovMatrix <- M4S_EstimateCovariance(myInput, myEstimate)
    
    myRecommendation <- M4S_DoseRecommendation(myInput, myEstimate, myCovMatrix, Dose= Dose , Tau= Tau , TInf= TInf , MIC=MIC , N=N) 
    myRecommendation$error=0
    observations[observations < LLOQ] <- LLOQ
    observations[observations > ULOQ] <- ULOQ
    pred <- M4S_FindC(c(0.025, 0.975), observationTimes, myInput, myEstimate, myCovMatrix, N=N*2)
    likely <- pred[1,] <= observations & observations <= pred[2,]
    if(any(!likely)) {
      i <- which(!likely)[1]
      myRecommendation$comment <- paste0(
        "WARNING: Concentration at t=", observationTimes[i], " of ",observations[i], " is very unlikely (Prediction 95%CI=[", round(pred[1,i], 2), "-", round(pred[2,i], 2), "]), proceed with caution!\n",
                                         myRecommendation$comment)
    }
    
    return(myRecommendation)
  }, error=function(e) {      
    
    myRecommendation<-data.frame(Dose=0, Tau=0, TInf=0,
                                pSafety=0,
                                pEfficacy=0,
                                safety=FALSE,
                                efficacy=FALSE,
                                selected=FALSE,
                                comment=e$message,
                                error=1 ) 
    myRecommendation$Dose=0
    myRecommendation$Tau=0
    myRecommendation$TInf=0
    myRecommendation$pSafety=0
    myRecommendation$pEfficacy=0
    myRecommendation$error=1
    myRecommendation$comment=e$message
    return (myRecommendation)      
   }) 
}

M4S_CheckCovariates <- function(covariates) {
  BW <- covariates[1]
  CRCL <- covariates[2]
  if(BW < 30) stop("BodyWeight supplied is lower than 30kg; are you sure there is no typing error?")
  if(BW > 300) stop("BodyWeight supplied is higher than 300; are you sure there is no typing error?")
  if(CRCL < 10) stop("Creatinine clearance lower than 10 mL/min; subject probably in renal failure?")
  if(CRCL > 1000) stop("Creatinine clearance higher than 1000 mL/min; model cannot run")
}

# Generate an input datafile
# Example usage: input <- M4S_GenerateInput(
#                   observationTimes=c(0.2, 1.5, 4.95, 8.40), # hours after t0. there should ALWAYS be an observation before the first treatment
#                   observations=c(0.5, 51.15582, 21.51408, 11.85096), #in mg/l, should be between LLOQ(=0.5) and ULOQ(=200.0).
#                   covariates=c(75, 80),  # 75 kg, 80 ml/min creatinine clearance
#                   treatmentTimes=c(0.5, 8.5, 16.5), # hours after t0
#                   treatmentDoses=c(2000, 2000, 2000), # doses of compound in mg
#                   treatmentInfDuration=c(0.5, 0.5, 0.5) ) # infusion duration in hours
# Observations higher than ULOQ (upper limit of quantification = 0.5) or lower than LLOQ (lower limit of quantification = 200.0) should be reported as
# 0.5 or 200.
# Treatments before t=0 should not be provided
# Observations before t=0 should not be provided
# ! This method does not filter observations that are a long time ago...
M4S_GenerateInput <- function(
                      observationTimes, 
                      observations=NULL, 
                      covariates,
                      treatmentTimes,
                      treatmentDoses,
                      treatmentInfDuration,
                      realParameters=numeric(),
                      addError=TRUE
          ) {
  M4S_CheckCovariates(covariates)
  if(is.null(realParameters)) {
    mu <- sapply(Parameters, function(x){x@mu})
    sd <- sapply(Parameters, function(x){x@sd})
    realParameters <- rnorm(length(mu), mu, sd)
  }
  
  if(length(treatmentTimes) != length(treatmentDoses)) stop("TreatmentTimes do not match treatmentDoses")
  if(length(treatmentTimes) != length(treatmentInfDuration)) stop("TreatmentTimes do not match treatmentInfDuration")
  
  names(covariates) <- c()
  events <- lapply(
    seq(1, length(treatmentTimes)), 
    function(i) { 
      dose(t=treatmentTimes[i],treatmentDoses[i],duration=treatmentInfDuration[i])
      } )
  if(sum(0==observationTimes) != 1) stop("We need a single residual beta-lactam level for t=0!")
  if(is.null(observations) && !is.null(observationTimes)) {
    # make them!
    myInput <- resultsData(
      subject=1,
      data=data.frame(t=0, x=0),
      ebe=data.frame(t=0, x=0),
      covariates=covariates,
      events=events,
      realParameters=realParameters
    ) 
    eta <- myInput@realParameters
    if(length(eta) != length(Parameters)) stop("We cannot generate observations if no realParameters were supplied...")
    observations <- Model(observationTimes, myInput, eta)
    if(addError)
      observations <- observations + rnorm(length(observations), mean=0, sd=sqrt((EPS_ADD)**2 + (EPS_PROP*observations)**2) )
    observations[observations < LLOQ] <- LLOQ
    observations[observations > ULOQ] <- ULOQ
  }
  if(length(observationTimes) != length(observations)) stop("ObservationTimes do not match observations")
  myData <- data.frame(t=observationTimes, x=observations)
  
  resultsData(
    subject=1,
    data=myData,
    ebe=myData[-1, ], # first row is t0
    covariates=covariates,
    events=events,
    realParameters=realParameters,
    treatmentTimes=treatmentTimes,
    treatmentDoses=treatmentDoses,
    treatmentInfDuration=treatmentInfDuration
  )
}

# Do PK parameter estimation, based on an input file
# Expects: input generated by M4S_GenerateInput or M4S_ReadInput
# Returns: vector with estimates for ETA parameters
M4S_Estimate <- function(input) {
  myModel <- function(tPred, eta) {
    Model(tPred, input, eta)
  }
  myEstimates <- ebe(myModel, Parameters, input@ebe)
  myEstimates
}

# Estimate variability of an estimation
# Expects: input generated by M4S_GenerateInput or M4S_ReadInput
# Expects: estimates generated by M4S_Estimate
# Returns: variance/covariance matrix
M4S_EstimateCovariance <- function(input, myEstimate) {
  myModel <- function(tPred, eta) {
    Model(tPred, input, eta)
  }
  covMatrix <- ebe_sd(myEstimate, myModel, Parameters, input@ebe)
  sdZeroPars <- sapply(Parameters, function(x) x@sd == 0)
  if(det(covMatrix[!sdZeroPars, !sdZeroPars]) == 0) {
    stop("Covariance matrix determinant is 0...")
  }
  covMatrix
}


# Find the dose recommendation
# Expects: numeric vector: which p_values to estimate a dose for (e.g: c(0.80, 0.95)  )
# Expects: input generated by M4S_GenerateInput or M4S_ReadInput
# Expects: estimates generated by M4S_Estimate
# Expects: variance/covariance matrix generated by M4S_DrawEstimatePlot
# N: number of monte carlo samples to take
# DoseUpperLimit: maximum limit for dose
# SSTau: dose interval for maintenance dose
# SSDuration: infusion duration for maintenance dose
# MIC: the MIC
M4S_FindDose <- function(qProbs, input, myEstimates, covMatrix, N=600, DoseUpperLimit=500*1000, SSTau=8, SSDuration=1, MIC) {
  if(N <= 1) {
    return(
      findD(myEstimates, SSTau, SSDuration, input, SSModel, DoseUpperLimit, MIC)
    )
  }
  MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
  Dose <- M4SParApply(MCEstParameters, 1, findD, 
                   SSTau, SSDuration, input, SSModel, DoseUpperLimit, MIC)
  
  qDose <- quantile(Dose, probs=qProbs, na.rm=TRUE) #remove samples which did not contain values
  qDose
}

# For a specific dose, find the Cmax associated during steady state
# Expects: numeric vector: which p_values to estimate Cmax for (e.g: c(0.80, 0.95)  ). 
#   The higher this value, the higher the Cmax.
# Expects: dose for which to calculate the Cmax
# Expects: input generated by M4S_GenerateInput or M4S_ReadInput
# Expects: estimates generated by M4S_Estimate
# Expects: variance/covariance matrix generated by M4S_DrawEstimatePlot
# N: number of monte carlo samples to take
# DoseUpperLimit: maximum limit for dose
# SSTau: dose interval for maintenance dose
# SSDuration: infusion duration for maintenance dose
M4S_FindCmax <- function(qProbs, Dose, input, myEstimates, covMatrix, N=600, SSTau=8, SSDuration=1) {
  Cmax <- function(eta, input, Dose, tau, duration) {
    names(eta) <- names(Parameters)
    Pred <- SSModel(
      c(duration), 
      input,
      eta,
      tau,
      Dose,
      duration)
    Pred
  }
  if(N <= 1 || all(covMatrix==0)) {
    return(Cmax(myEstimates, input, Dose, SSTau, SSDuration))
  }
  MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
  
  CmaxValues <- M4SParApply(MCEstParameters, 1, Cmax, input, Dose, SSTau, SSDuration)
  qCmax <- quantile(CmaxValues, probs=qProbs, na.rm=T)
  qCmax
}

M4S_FindC <- function(qProbs, times, input, myEstimates, covMatrix, N=600) {
  myModel <- function(eta, times, input) {
    Model(times, input, eta)
  }
  MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
  CValues <- M4SParApply(MCEstParameters, 1, myModel, times, input)
  q <- apply(CValues, 1, quantile, probs=qProbs, na.rm=T)
  q
}

M4S_FindCmin <- function(qProbs, Dose, input, myEstimates, covMatrix, N=600, SSTau=8, SSDuration=1) {
  MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
  
  Cmin <- function(eta, input, Dose, tau, duration) {
    names(eta) <- names(Parameters)
    SSModel(0, input, eta, tau, Dose, duration)
  }
  
  CminValues <- M4SParApply(MCEstParameters, 1, Cmin, input, Dose, SSTau, SSDuration)
  
  qCmin <- quantile(CminValues, probs=qProbs, na.rm=T)
  qCmin
}

# For a specific dose, show the chance that fT >= the given value
M4S_FTProbability <- function(FTValue, Dose, input, myEstimates, covMatrix, N=600, POINTS=500, SSTau=8, SSDuration=1, MIC) {
  if(FTValue == 1) {
    # If we want the chance that FT = 1, we just need to find the number of people with CMin > 4MIC
    M4S_CMinProbability(4*MIC, Dose, input, myEstimates, covMatrix, N=N, SSTau, SSDuration)
  } else {
    MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
    fTAboveMIC <- function(eta, input, Dose, tau, duration, MIC) {
      names(eta) <- names(Parameters)
      Pred <- SSModel(seq(-tau, 0, length.out=POINTS), input, eta, tau, Dose, duration)
      mean( Pred > 4*MIC , na.rm=T) # This calculates fT for the given subject
    }
    fTAboveMICValues <- M4SParApply(MCEstParameters, 1, fTAboveMIC, input, Dose, SSTau, SSDuration, MIC)
    fTAboveMICValues <- omit.na(unlist(fTAboveMICValues)) # A whole lot of fT values
    cpdf <- ecdf(fTAboveMICValues) # Cumulative distribution function
    1 - cpdf(FTValue - .Machine$double.eps)
  }
}

M4S_CMinProbability <- function(CONC, Dose, input, myEstimates, covMatrix, N=600, SSTau=8, SSDuration=1) {
  MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
  CMin <- function(eta, input, Dose, tau, duration) {
    names(eta) <- names(Parameters)
    SSModel(0, input, eta, tau, Dose, duration)
  }
  CMinValues <- M4SParApply(MCEstParameters, 1, CMin, input, Dose, SSTau, SSDuration)
  CMinValues <- unlist(CMinValues) # A whole lot of CMin values
  mean( CMinValues > CONC , na.rm=T)
}

# For a specific dose, show the probability that CMax is below the given value
M4S_CmaxProbability <- function(CMaxValue, Dose, input, myEstimates, covMatrix, N=600, SSTau=8, SSDuration=1) {
  MCEstParameters <- mvrnorm(N, myEstimates, covMatrix)
  Cmax <- function(eta, input, Dose, tau, duration) {
    names(eta) <- names(Parameters)
    Pred <- SSModel(c(duration), input, eta, tau, Dose, duration)
    Pred
  }
  CmaxValues <- M4SParApply(MCEstParameters, 1, Cmax, input, Dose, SSTau, SSDuration)
  CmaxValues <- na.omit(unlist(CmaxValues))
  cpdf <- ecdf(CmaxValues)
  # cpdf increases from low to high
  # so the higher CMaxValue, the higher this chance
  cpdf(CMaxValue)
}


# Returns all possible dosing regimens, sorted by preference
# myInput: input generated by M4S_GenerateInput or M4S_ReadInput
# myEstimate: estimates generated by M4S_Estimate
# myCovMatrix: variance/covariance matrix generated by M4S_DrawEstimatePlot
# Dose, Tau, TInf: current treatment
# optimize: q_upper_cmin needs to be calculated for all regimen. This setting only determines if we can compare
# the daily dose of an intermittend infusion with the theoretical dose required for a continuous infusion.
# If it is less, the regimen will definitely not be efficacious.
# N: number of monte carlo samples to take
Possibilities <- data.frame(Dose=c(1000, 2000))
Possibilities <- merge(Possibilities, data.frame(Tau=c(6,8,12,24)))
Possibilities <- merge(Possibilities, data.frame(TInf=c(0.5, 3)))
Possibilities <- subset(Possibilities, Dose*24/Tau <= 6000) #max 6g daily
SAFETY_CMAX_Q <- 0.90
SAFETY_CMIN_Q <- 0.90
EFFICACY_CMIN_Q <- 0.90
FT_TARGET <- 1 #100%

M4S_DoseRecommendation <- function(input, estimate, covMatrix, Dose, Tau, TInf, MIC, optimize=FALSE, all=FALSE, N=1000) {
  sdZeroPars <- sapply(Parameters, function(x) x@sd == 0)
  if(N>1 && det(covMatrix[!sdZeroPars, !sdZeroPars]) == 0) {
    stop("Covariance matrix determinant is 0...")
  }
  
  # a dose can never be efficient if it is a lower daily dose than the required continuous infusion dose
  ContInfDailyDose <- M4S_FindDose(EFFICACY_CMIN_Q, input, estimate, covMatrix, N=N, DoseUpperLimit=500*1000, SSTau=24, SSDuration=24, MIC)
  q_cmax <- function(row) { #Calculates upper limit for CMax; to check for safety
    Dose<-as.numeric(row['Dose'])
    Tau<-as.numeric(row['Tau'])
    TInf<-as.numeric(row['TInf'])
    M4S_FindCmax(qProbs=SAFETY_CMAX_Q, 
                 Dose=Dose, 
                 input, 
                 estimate, 
                 covMatrix, 
                 N=N, 
                 SSTau=Tau, 
                 SSDuration=TInf)
  }
  q_cmin <- function(row) { #Calculates lower limit for CMin; to check for efficacy
    Dose<-as.numeric(row['Dose'])
    Tau<-as.numeric(row['Tau'])
    TInf<-as.numeric(row['TInf'])
    M4S_FindCmin(qProbs=1-EFFICACY_CMIN_Q, 
                 Dose=Dose, 
                 input, 
                 estimate, 
                 covMatrix, 
                 N=N, 
                 SSTau=Tau, 
                 SSDuration=TInf)
  }
  q_upper_cmin <- function(row) { #Calculates upper limit for CMin; to distinguish between multiple good regimen
    Dose<-as.numeric(row['Dose'])
    Tau<-as.numeric(row['Tau'])
    TInf<-as.numeric(row['TInf'])
    M4S_FindCmin(qProbs=SAFETY_CMIN_Q, 
                 Dose=Dose, 
                 input, 
                 estimate, 
                 covMatrix, 
                 N=N, 
                 SSTau=Tau, 
                 SSDuration=TInf)
  }
  safety <- function(row) {
    M4S_CmaxProbability(
      SAFETY_CMAX,
      row['Dose'],
      input,
      estimate,
      covMatrix,
      N=N,
      SSTau=row['Tau'],
      SSDuration=row['TInf'])
  }
  efficacy <- function(row) {
    if(optimize && (row$Dose * 24/row$Tau) < ContInfDailyDose) return(0)
    M4S_FTProbability(
      FT_TARGET, #100% above 4*MIC
      row['Dose'],
      input,
      estimate,
      covMatrix,
      N=N,
      SSTau=row['Tau'],
      SSDuration=row['TInf'],
      MIC=MIC)
  }
  calculate <- function(rows) {
    calc <- rows
    rows <- within(calc, {
      pSafety <- apply(calc, 1, safety)
      pEfficacy <- apply(calc, 1, efficacy)
      safety    <- pSafety > SAFETY_CMAX_Q
      efficacy  <- pEfficacy > EFFICACY_CMIN_Q
      selected <- safety & efficacy
      comment <- ifelse(safety & efficacy, "Safe and efficient",
                    ifelse(safety, "Safe, NOT efficient",
                    ifelse(efficacy, "NOT safe, efficient",
                           "NOT safe NOT efficient"
                           )))
      q_cmax <- apply(calc, 1, q_cmax)
      q_cmin <- apply(calc, 1, q_cmin)
      q_upper_cmin <- apply(calc, 1, q_upper_cmin)
    })
    return(rows)
  }
  
  # Ordering criterium:
  # Always pick the regimen with lowest predicted CMin (q_upper_cmin)
  p <- calculate(Possibilities)
  if(sum(p$selected, na.rm=T) >= 1) { #One or more options possible
    # Return the one with lowest q_upper_cmin
    
    # If multiple regimens match, the first one is the preferred one anyway
    i <- which.min( ifelse(p$selected, p$q_upper_cmin, Inf) ) #Only pick from the selected regimen
    # reset all other regimens
    p$selected <- FALSE
    p$selected[i] <- TRUE
    
    return( if(all) p else p[i, ] )
  } else {
    # no dosing regimen are valid
    # 3 possibilities:
    
    # 1) There are no efficient regimes. Every CMin is below 4*MIC. We need to dose higher...
    if(sum(p$efficacy) == 0) {
      # Propose a continuous infusion
      if(ContInfDailyDose > 6000) {
        if(MIC==2) {
          comment = paste0("Continuous infusion capped at 6g/day
(should be ", round(ContInfDailyDose), "mg/day)")
          ContInfDailyDose = 6000
        } else {
          comment = paste0("Continuous infusion, high daily dose >6g/day")
        }
      } else {
        comment = "Continuous infusion, high daily dose"
      }
      TInf <- 4  #max(p$TInf)
      Dose <- ContInfDailyDose / 24 * TInf
      Dose <- ceiling(Dose)
      
      row <- data.frame(Dose=Dose, Tau=TInf, TInf=TInf)
      row <- calculate(row)
      row$comment <- comment
      p <- rbind(p, row)
      
      return( if(all) p else row )
    } else if (sum(p$safety) == 0) {
      #2) There are no safe regimes: 1g q24h is too much. Allow 0.5g and allow 48h.
      row <- calculate(data.frame(Dose=500, Tau=24, TInf=3 ))
      row$comment <- "Safe regime only found at very low dose, patient in renal failure?"
      if(row$safety) row$selected <- TRUE
      p <- rbind(p, row)
      if(row$safety) {
        return( if(all) p else row )
      }
      row <- calculate(data.frame(Dose=1000, Tau=48, TInf=3 ))
      row$comment <- "Safe regime only found at very low dose, patient in renal failure?"
      if(row$safety) row$selected <- TRUE
      p <- rbind(p, row)
      if(row$safety) {
        return( if(all) p else row )
      }
      row <- calculate(data.frame(Dose=500, Tau=48, TInf=max(p$TInf) ))
      row$comment <- "Safe regime only found at very low dose, patient in renal failure?"
      if(row$safety) row$selected <- TRUE
      p <- rbind(p, row)
      if(row$safety) {
        return( if(all) p else row )
      }

      # TODO: error
      stop("Could not find any safe regimen!")
    } else {
      # Propose a continuous infusion
      TInf <- 4  #max(p$TInf)
      Dose <- ContInfDailyDose / 24 * TInf
      Dose <- ceiling(Dose)
      
      row <- data.frame(Dose=Dose, Tau=TInf, TInf=TInf)
      row <- calculate(row)
      row$comment <- "Continuous infusion, to dose more exact"
      p <- rbind(p, row)
      
      return( if(all) p else row )
    }
  }
}