source('api.r')
source('api_extra.r')
source('meropenem.r')
#Scenario 1a: normal operation
#M4S_DrawMachineRecommendation <- M4S_MachineRecommendation
png()
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 7, 9),
  observations=c(0, 10, 60),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)


M4S_DrawMachineRecommendation(
  observationTimes=c(0, 7),
  observations=c(0, 10),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)

M4S_DrawMachineRecommendation(
  observationTimes=c(0),
  observations=c(0),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)

dev.off()


#Scenario 1b: normal operation
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 35.5, 39.5),
  observations=c(0, 50, 30),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16, 24, 32, 40),
  treatmentDoses=c(2, 1, 1, 2, 2, 2)*1000,
  treatmentInfDuration=c(0.5, 0.5, 0.5, 3, 3, 3),
  Dose=2000,
  Tau=8,
  TInf=3,
  MIC=2,
  N=1000
)

# Scenario 1c: fat person
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 9, 15),
  observations=c(0, 60, 2),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)


#Scenario 2: Bad input
M4S_MachineRecommendation(
  observationTimes=c(0, 15, 17),
  observations=c(0, 10, 60),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8),
  treatmentDoses=c(2000, 0),
  treatmentInfDuration=c(0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)

M4S_DrawMachineRecommendation(
  observationTimes=c(0, 15, 17),
  observations=c(0, 10, 60),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8),
  treatmentDoses=c(2000, 0),
  treatmentInfDuration=c(0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)


#Scenario 3b: dialysis, 2 concentrations
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 2.5),
  observations=c(38, 30),
  covariates=c(70, 100),
  treatmentTimes=c(3),
  treatmentDoses=c(2)*1000,
  treatmentInfDuration=c(3),
  Dose=2000,
  Tau=8,
  TInf=3,
  MIC=2,
  N=1000
)


#Scenario 3c: dialysis, 3 concentrations
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 2.5, 6.5),
  observations=c(38, 30, 50),
  covariates=c(70, 100),
  treatmentTimes=c(3),
  treatmentDoses=c(2)*1000,
  treatmentInfDuration=c(3),
  Dose=NA,
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)

#Scenario 4: only 1 observation
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 9),
  observations=c(0, 60),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=2,
  N=1000
)


#Scenario 5a
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 9, 15),
  observations=c(0, 60, 10),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=8,
  N=1000
)

#Scenario 5b
M4S_DrawMachineRecommendation(
  observationTimes=c(0, 9),
  observations=c(0, 70),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=8,
  N=1000
)


M4S_DrawMachineRecommendation(
  observationTimes=c(0, 9),
  observations=c(0, 70),
  covariates=c(70, 100),
  treatmentTimes=c(0, 8, 16),
  treatmentDoses=c(2000, 1000, 1000),
  treatmentInfDuration=c(0.5, 0.5, 0.5),
  Dose='NA',
  Tau='NA',
  TInf='NA',
  MIC=8,
  N=1000
)
