# README #

This code shows the current Mon4strat dose adjustment algorithm.

### License ###
This work is licensed under the CC BY-NC-ND v3.0. 

You are free to:
Share — copy and redistribute the material in any medium or format
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
NonCommercial — You may not use the material for commercial purposes.
NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

More information: http://creativecommons.org/licenses/by-nc-nd/3.0/

### What is this repository for? ###
This repository incorporates dosing rules for the Mon4strat dosing adjustment, based on the 2cpt beta-lactam model in ICU by [Delattre 2010](http://www.sciencedirect.com/science/article/pii/S0009912009005554).

### Contribution guidelines ###
This project needs to be ported to a separate R package. This is no small task, since both the 2cpt-model and the parameter distributions need to be abstracted.

### Who do I talk to? ###

* Original author: ruben.faelens@sgs.com